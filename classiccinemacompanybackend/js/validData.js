const validTitles = [`Ms`, `Miss`, `Mrs`, `Mr`, `Dr`];
const validGenders = [`Female`, `Male`, `female`, `male`];

module.exports = { validTitles, validGenders };