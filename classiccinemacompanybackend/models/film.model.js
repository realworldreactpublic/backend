const mongoose = require(`mongoose`);
const Schema = mongoose.Schema;

const { isoDateRegExp } = require(`../js/regExps`);

const Film = new Schema({
    title: { type: String, required: true },
    synopsis: { type: String, required: true },
    cast: { type: String, required: true },
    directors: { type: String, required: true },
    showingTimes: { type: String, required: true },
    releaseDate: { type: Date, required: true, match: [isoDateRegExp, `is invalid`] },
    filmStatus: { type: Number, min: 1, max: 2 },
    img: { type: String, required: true }
});

module.exports = mongoose.model(`Film`, Film);

