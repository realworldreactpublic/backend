const express = require("express");
const app = express();
const mongoose = require("mongoose");
const PORT = 4000;
const DBURI = `mongodb://localhost:27017/qacinemas`; // For dev
// const DBURI = `mongodb://mongo:27017/cinema`; // For docker
const DB = mongoose.connection;
const allFilms = require("./routes/allFilms");
const openingTimes = require("./routes/openingTimes");
const singleFilm = require("./routes/singleFilm");
const makeBooking = require("./routes/makeBooking");
const signup = require(`./routes/signup`);
const cors = require(`cors`);

const HTTPS = require(`https`);
const FS = require(`fs`);

app.use(cors());
app.use(`/films`, allFilms);
app.use(`/openingTimes`, openingTimes);
app.use(`/singleFilm`, singleFilm);
app.use(`/makeBooking`, makeBooking);
app.use(`/signup`, signup);

// Written to allow Mongo Docker container to have copied the cinema db from the host
// before connecting to the Database
const dbConnection = () => {
  setTimeout(() => {
    mongoose
      .connect(DBURI, { useNewUrlParser: true }, () => {})
      .then(() => console.log(`Connection to DB successful`))
      .catch(error => {
        console.log(
          `Attempt to connect to database failed: ${error}; Trying again...`
        );
        dbConnection();
      });
  }, 20000);
};

dbConnection();

const server = app.listen(PORT, () => {
  const SERVERHOST = server.address().address;
  const SERVERPORT = server.address().port;

  console.log(`Server is running on http://${SERVERHOST}:${SERVERPORT}`);
});

// const httpsOptions = {
//   key: FS.readFileSync(`server.key`),
//   cert: FS.readFileSync(`server.cert`)
// };

// const server = HTTPS.createServer(httpsOptions, app).listen(PORT, () => {
//   const SERVERHOST = server.address().address;
//   const SERVERPORT = server.address().port;
//   console.log(`Server is running on https://${SERVERHOST}:${SERVERPORT}`);
// });

module.exports = server;
