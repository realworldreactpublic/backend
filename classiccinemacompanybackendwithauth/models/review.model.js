const mongoose = require(`mongoose`);
const Schema = mongoose.Schema;

const Review = new Schema({
    filmId: { type: String},
    filmTitle: {type: String },
    description: { type: String, required: true },
    score: {type: Number, min: 1, max: 5},
    username: {type: String},
    dateCreated: {type: Date, default: Date.now()}
});

module.exports = mongoose.model(`review`, Review);

