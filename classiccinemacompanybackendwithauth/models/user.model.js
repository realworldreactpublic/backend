const mongoose = require(`mongoose`);
const Schema = mongoose.Schema;
const { emailRegExp, phoneNumberRegExp } = require(`../js/regExps`);

const { validTitles, validGenders } = require(`../js/validData`);

const passportLocalMongoose = require('passport-local-mongoose');

const User = new Schema({
    username: {
        type: String,
        required: true,
        unique: true
    },
    title: {
        type: String, required: [true, `No title supplied`], validate: {
            validator: value => validTitles.includes(value),
            message: `Not a valid title`
        }
    },
    firstName: {
        type: String, required: [true, `No first name supplied`], validate: {
            validator: value => {
                return value.length > 1;
            },
            message: `First name is not long enough`
        }
    },
    lastName: {
        type: String, required: [true, `No last name supplied`], validate: {
            validator: value => {
                return value.length > 1;
            },
            message: `Last Name is not long enough`
        }
    },
    email: { type: String, required: [true, `No email supplied`], match: [emailRegExp, `Invalid Email format`] },
    phoneNumber: { type: String, match: [phoneNumberRegExp, `Invalid phone number supplied`], required: false },
    dob: {
        type: Date,
        required: false
    },
    gender: {
        type: String, validate: {
            validator: value => {
                if (!value) {
                    return true;
                } else {
                    return validGenders.includes(value);
                }
            },
            message: `Not a valid gender`
        },
        required: false
    }
});

User.plugin(passportLocalMongoose);

module.exports = mongoose.model(`User`, User), { validTitles, validGenders };