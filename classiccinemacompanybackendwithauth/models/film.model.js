const mongoose = require(`mongoose`);
const Schema = mongoose.Schema;

const { isoDateRegExp } = require(`../js/regExps`);

const Film = new Schema({
    title: { type: String, required: true },
    synopsis: { type: String, required: true },
    shortName: { type: String, required: true },
    // cast: { type: String, required: true },
    // directors: { type: String, required: true },
    showTimes: { type: Array, required: true },
    // releaseDate: { type: Date, required: true, match: [isoDateRegExp, `is invalid`] },
    // filmStatus: { type: Number, min: 1, max: 2 },
    // img: { type: String, required: true }
    ageCert: {type: String, required: true },
    nowShowingSrcset: {type: Object, required: true},
    whatsOnSrcset: {type: Object, required: true}
});

module.exports = mongoose.model(`film`, Film);

