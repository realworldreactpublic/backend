const express = require(`express`);
const app = express();
const mongoose = require(`mongoose`);
const PORT = 4000;
const DBURI = `mongodb+srv://root:A9t8C8kJWEenvsJ5@cluster0.pey95.mongodb.net/qacinemas?retryWrites=true&w=majority`; // For dev
// const DBURI = `mongodb://mongo:27017/cinema`; // For docker
const DB = mongoose.connection;
const allFilms = require(`./routes/allFilms`);
const openingTimes = require(`./routes/openingTimes`);
const singleFilm = require(`./routes/singleFilm`);
const makeBooking = require(`./routes/makeBooking`);
const reviews = require(`./routes/reviews`);
const users = require(`./routes/users`);
const cors = require(`cors`);

const HTTPS = require(`https`);
const FS = require(`fs`);

const cookieParser = require('cookie-parser');
const passport = require(`passport`);
const LocalStrategy = require(`passport-local`).Strategy;
const {ALGORITHM, SECRET} = require(`./config/config.json`);

app.use(cookieParser());
app.use(passport.initialize());
app.use(passport.session());

const User = require(`./models/user.model`);
passport.use(new LocalStrategy(User.authenticate()));
passport.serializeUser(User.serializeUser());
passport.deserializeUser(User.deserializeUser());

const { Strategy: JwtStrategy, ExtractJwt } = require(`passport-jwt`);
const opts = {
  jwtFromRequest: ExtractJwt.fromAuthHeaderAsBearerToken(),
  secretOrKey: SECRET,
  algorithms: [ALGORITHM]
};

passport.use(new JwtStrategy(opts, function (jwt_payload, done) {
  User.findById(jwt_payload.sub, function (err, user) {
      console.log(err, user);
      if (err) {
          return done(err, false);
      }
      if (user) {
          return done(null, user);
      } else {
          return done(null, false);
      }
  });
}));

app.use(cors());
app.use(`/films`, allFilms);
app.use(`/openingTimes`, openingTimes);
app.use(`/singleFilm`, singleFilm);
app.use(`/makeBooking`, makeBooking);
app.use(`/users`, users);
app.use(`/reviews`, reviews);

// Written to allow Mongo Docker container to have copied the cinema db from the host
// before connecting to the Database
const dbConnection = () => {
  // setTimeout(() => {
    mongoose
      .connect(DBURI, { useNewUrlParser: true, useUnifiedTopology: true })
      .then(() => console.log(`Connection to DB successful`))
      .catch(error => {
        console.log(
          `Attempt to connect to database failed: ${error}; Trying again...`
        );
        dbConnection();
      });
  // }, 20000);
};

dbConnection();

const server = app.listen(PORT, () => {
  const SERVERHOST = server.address().address;
  const SERVERPORT = server.address().port;

  console.log(`Server is running on http://${SERVERHOST}:${SERVERPORT}`);
});

// const httpsOptions = {
//   key: FS.readFileSync(`server.key`),
//   cert: FS.readFileSync(`server.cert`)
// };

// const server = HTTPS.createServer(httpsOptions, app).listen(PORT, () => {
//   const SERVERHOST = server.address().address;
//   const SERVERPORT = server.address().port;
//   console.log(`Server is running on https://${SERVERHOST}:${SERVERPORT}`);
// });

module.exports = server;
