const express = require(`express`);
const router = express.Router();
const Review = require(`../models/review.model`);
const { check, validationResult } = require(`express-validator/check`);
const bodyParser = require(`body-parser`);

router.use(bodyParser.json());


router.route(`/`).get((req, res) => {
    Review.find((error, reviews) => {
        error ? res.status(404).json({ 'message': `Not found` }) : res.json(reviews);
    });

});

router.delete('/remove/:id', (req, res) => {
    const {id} = req.params;
    
    Review.findByIdAndDelete(id, (err, doc) => {
        if (err) {
            return res.status(500).send(err.message);
        }
        return res.json(doc);
    })
});

router.post('/create', 
        [
            check('description').exists(),
            check('score').exists()
        ], 
        (req, res) => {
            const errors = validationResult(req);
            if (!errors.isEmpty()) {
                return res.status(422).json({
                    'message': `There were errors in the sign up data`,
                    'error': errors.array()
                });
            }
            const {body} = req;
            new Review(body).save()
                .then((doc) => res.status(201).json(doc))
                .catch((err) => res.status(500).send(err.message));
});

module.exports = router;